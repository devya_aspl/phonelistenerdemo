package com.phonelistenerdemo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.util.Date;

public class ServiceReceiver extends BroadcastReceiver {

    TelephonyManager telManager;
    Context context;
    private boolean startedCall = false; // New added boolean

    private static int lastState = TelephonyManager.CALL_STATE_IDLE;
    private static Date callStartTime;
    private static boolean isIncoming;
    private static String savedNumber;


    @Override
    public void onReceive(final Context context, Intent intent) {
//        // TODO: This method is called when the BroadcastReceiver is receiving
//        // an Intent broadcast.
//        throw new UnsupportedOperationException("Not yet implemented");

//        TelephonyManager telephony = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
//        telephony.listen(new PhoneStateListener(){
//            @Override
//            public void onCallStateChanged(int state, String incomingNumber) {
//                super.onCallStateChanged(state, incomingNumber);
//                System.out.println("incomingNumber : "+incomingNumber);
//                Log.e("receiver","incomingNumber : " + incomingNumber);
//                Toast.makeText(context,"incomingNumber : " + incomingNumber,Toast.LENGTH_SHORT);
//            }
//        },PhoneStateListener.LISTEN_CALL_STATE);
//
//        this.context=context;
//        startedCall = false; // New added boolean
//        telManager = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
//        telManager.listen(phoneListener, PhoneStateListener.LISTEN_CALL_STATE);

        if (intent.getAction().equals("android.intent.action.NEW_OUTGOING_CALL")) {
            savedNumber = intent.getExtras().getString("android.intent.extra.PHONE_NUMBER");

        }
        else{
            String stateStr = intent.getExtras().getString(TelephonyManager.EXTRA_STATE);
            String number = intent.getExtras().getString(TelephonyManager.EXTRA_INCOMING_NUMBER);
            int state = 0;
            if(stateStr.equals(TelephonyManager.EXTRA_STATE_IDLE)){
                state = TelephonyManager.CALL_STATE_IDLE;
            }
            else if(stateStr.equals(TelephonyManager.EXTRA_STATE_OFFHOOK)){
                state = TelephonyManager.CALL_STATE_OFFHOOK;
            }
            else if(stateStr.equals(TelephonyManager.EXTRA_STATE_RINGING)){
                state = TelephonyManager.CALL_STATE_RINGING;
            }

            onCallStateChanged(context, state, number);
        }
    }

    private final PhoneStateListener phoneListener = new PhoneStateListener() {
        @Override
        public void onCallStateChanged(int state, String incomingNumber) {
            super.onCallStateChanged(state, incomingNumber);
            try {
                switch (state) {
                    case TelephonyManager.CALL_STATE_RINGING: {
                        Toast.makeText(context,"Incoming call",Toast.LENGTH_SHORT).show();
                        if(incomingNumber!=null) {
                            Toast.makeText(context,"Incoming call number: "+incomingNumber,Toast.LENGTH_SHORT).show();
                            Log.e("receiver","Incoming call ");
                            Log.e("receiver","Incoming call number: "+incomingNumber);
                            //incoming call
//                            MainActivity.stopMP()
                        }
                        break;
                    }
                    case TelephonyManager.CALL_STATE_OFFHOOK: {
                        Toast.makeText(context,"outgoing call",Toast.LENGTH_SHORT).show();
//                        startedCall  = true; // Newly added code
                        if(incomingNumber!=null) {
                            Toast.makeText(context,"outgoing call number: "+incomingNumber,Toast.LENGTH_SHORT).show();
                            Log.e("receiver","outgoing call ");
                            Log.e("receiver","outgoing call number: "+incomingNumber);
                            //outgoing call
//                            MainActivity.stopMP();
                        }
                        break;
                    }
                    case TelephonyManager.CALL_STATE_IDLE: {
                        Toast.makeText(context,"idle state",Toast.LENGTH_SHORT).show();
//                        if(startedCall) {
////                            MainActivity.titleMusic.setVisibility(View.VISIBLE);
////                            MainActivity.Play();
////                            MediaService.startMP();
//                        }
                        break;
                    }
                    default: { }
                }
            } catch (Exception ex) {

            }
        }
    };

    public void onCallStateChanged(Context context, int state, String number) {
        if(lastState == state){
            //No change, debounce extras
            return;
        }
        switch (state) {
            case TelephonyManager.CALL_STATE_RINGING:
                isIncoming = true;
                callStartTime = new Date();
                savedNumber = number;

                Toast.makeText(context, "Incoming Call Ringing" , Toast.LENGTH_SHORT).show();
                break;
            case TelephonyManager.CALL_STATE_OFFHOOK:
                //Transition of ringing->offhook are pickups of incoming calls.  Nothing done on them
                if(lastState != TelephonyManager.CALL_STATE_RINGING){
                    isIncoming = false;
                    callStartTime = new Date();
                    Toast.makeText(context, "Outgoing Call Started" , Toast.LENGTH_SHORT).show();
                }

                break;
            case TelephonyManager.CALL_STATE_IDLE:
                //Went to idle-  this is the end of a call.  What type depends on previous state(s)
                if(lastState == TelephonyManager.CALL_STATE_RINGING){
                    //Ring but no pickup-  a miss
                    Toast.makeText(context, "Ringing but no pickup" + savedNumber + " Call time " + callStartTime +" Date " + new Date() , Toast.LENGTH_SHORT).show();
                }
                else if(isIncoming){

                    Toast.makeText(context, "Incoming " + savedNumber + " Call time " + callStartTime  , Toast.LENGTH_SHORT).show();
                }
                else{

                    Toast.makeText(context, "outgoing " + savedNumber + " Call time " + callStartTime +" Date " + new Date() , Toast.LENGTH_SHORT).show();

                }

                break;
        }
        lastState = state;
    }

}
